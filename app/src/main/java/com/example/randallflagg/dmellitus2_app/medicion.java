package com.example.randallflagg.dmellitus2_app;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.IdRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;
import java.util.Date;
import java.text.DateFormat;
import android.widget.DatePicker;
import static android.R.attr.button;
import static android.widget.Toast.*;
import java.util.Calendar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

public class medicion extends AppCompatActivity  {
    RadioGroup tiempo, tipo;
    EditText valorg;
    Button aceptar;
    String tiempo2,tipo2, tipo3, tiempo3, valorglucosa, c;
    Integer tipo1, tiempo1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicion);
        tiempo = (RadioGroup)findViewById(R.id.tiempo);
        tipo = (RadioGroup)findViewById(R.id.tipo);
        valorg = (EditText)findViewById(R.id.et_valor);
        aceptar = (Button)findViewById(R.id.btn_guardavalores);
        //date = (DatePicker) findViewById(R.id.datePicker);


    }

    private void EnviarMensaje (String Numero, String Mensaje){
        try {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(Numero,null,Mensaje,null,null);
            Toast.makeText(getApplicationContext(), "Mensaje Enviado.", Toast.LENGTH_LONG).show();
        }

        catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Mensaje no enviado, datos incorrectos.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    public void btn_guardar_valores (View v){
        Date d = new Date();
        String fecha = d.toString();
        fecha =  DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(d);
        fecha = fecha.substring(0,8);
        //Toast.makeText(this, fecha, Toast.LENGTH_SHORT).show();
        base_de_datos db = new base_de_datos(this, "database", null, 1);
        final SQLiteDatabase data_base = db.getWritableDatabase();
        Integer tamaño;

        tiempo1 = tiempo.getCheckedRadioButtonId();
        tiempo2 = Integer.toString(tiempo1);
        tipo1 = tipo.getCheckedRadioButtonId();
        tipo2 = Integer.toString(tipo1);
        valorglucosa = valorg.getText().toString();

        if( tiempo1!=-1 && tipo1!=-1 && valorglucosa.length()>0 ) {

            if (tipo1 == R.id.rdbDesayuno){
                tipo3 = "desayuno";
        }
            else if (tipo1 == R.id.rdbComida) {
                tipo3 = "comida";
            }
            else if (tipo1 == R.id.rdbCena) {
                tipo3 = "cena";
            }else{
                tipo3 = "error";
            }

            if (tiempo1 == R.id.rdbAD) {
                tiempo3 = "antes de";
            }
            else if (tiempo1 == R.id.rdbDD) {
                tiempo3 = "despues de";
            }else{
                tiempo3 = "error";
            }
            /////ALERTAS
            //if (12 < valor de glucosa >32)
            if(Integer.parseInt(valorglucosa)<=80||Integer.parseInt(valorglucosa)>=140){
                String correo="";
                String c = "SELECT correo_d, telefono_d  FROM doctor_db ORDER BY doctor_id DESC LIMIT 1";
                Cursor consulta = data_base.rawQuery(c, null);
                if (consulta.moveToNext()) {
                    c = "INSERT INTO mediciones (tipo, tiempo, nivel,fecha,paciente_id) VALUES ('" + tipo3 + "','" + tiempo3 + "','" + valorglucosa + "','"+ fecha +"','"+DataHolder.getData()+"')";
                    data_base.execSQL(c);
                    //EnviarCorreo();
                    EnviarMensaje(consulta.getString(1),"Alerta! el paciente "+DataHolder.getName()+" tiene un nivel anormal de glucosa: "+valorglucosa+"(mg/dl)");
                    correo = consulta.getString(0);
                    String[] to = new String[]{correo};
                    Intent email = new Intent(Intent.ACTION_SEND);
                    email.putExtra(Intent.EXTRA_EMAIL  , new String[]{consulta.getString(0)});
                    email.putExtra(Intent.EXTRA_SUBJECT, "Alerta, el paciente "+DataHolder.getName()+" tiene un nivel anormal de glucosa"+valorglucosa+"(mg/dl)");
                    email.setType("message/rfc822");
                    try {
                        startActivity(Intent.createChooser(email, "Enviar e-mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(this, "No hay clientes e-mail instalados.", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(this, "Error",Toast.LENGTH_LONG).show();
                }
            }else{
                c = "INSERT INTO mediciones (tipo, tiempo, nivel,fecha,paciente_id) VALUES ('" + tipo3 + "','" + tiempo3 + "','" + valorglucosa + "','"+ fecha +"','"+DataHolder.getData()+"')";
                data_base.execSQL(c);
                //data_base.close();
                Toast.makeText(this, c, Toast.LENGTH_LONG).show();
            }
            data_base.close();
            //Insert into our db


        }
        else  {

            final AlertDialog.Builder welcome = new AlertDialog.Builder(this);
            welcome.setMessage("Debes completar todos los campos")
                    .setTitle("Aviso").setCancelable(false)
                    .setNeutralButton("Aceptar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


            }
        data_base.close();
    }


        }


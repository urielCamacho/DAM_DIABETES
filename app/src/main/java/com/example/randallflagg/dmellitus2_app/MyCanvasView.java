package com.example.randallflagg.dmellitus2_app;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by uriel on 30/11/17.
 */

class MyCanvasView extends View {
    public MyCanvasView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public MyCanvasView(Context context) {
        super(context);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRGB(255,255,255);
        int ancho=canvas.getWidth();
        Paint pincel1=new Paint();
        canvas.drawLine(40,70, 40,canvas.getHeight()-200,pincel1);
        canvas.drawLine(40,canvas.getHeight()-200, canvas.getWidth()-20,canvas.getHeight()-100,pincel1);

    }
}

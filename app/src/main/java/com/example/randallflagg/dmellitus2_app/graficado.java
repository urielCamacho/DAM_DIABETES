package com.example.randallflagg.dmellitus2_app;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.Spinner;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import android.widget.AdapterView.OnItemClickListener;


public class graficado extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Bitmap iconoBitmap;
    Drawable iconoDrawable;
    LinearLayout layot;
    int [] diary = new int[] {0,0,0,0,0,0,0};
    Date d;
    String fecha="";
    Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graficado);
        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Hoy");
        categories.add("Esta semana");
        categories.add("Esta mes");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        drawday();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }


    public void drawday(){

        d = new Date();
        fecha = d.toString();
        fecha =  DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(d);
        fecha = fecha.substring(0,8);
        //Toast.makeText(this,"Fecha:"+fecha,Toast.LENGTH_LONG).show();
        base_de_datos db = new base_de_datos(this, "database", null, 1);
        final SQLiteDatabase data_base = db.getReadableDatabase();
        //Toast.makeText(this,"idpaciente: "+DataHolder.getData()+", fecha:"+fecha,Toast.LENGTH_LONG).show();
        String c = "SELECT tipo, tiempo,nivel  FROM mediciones WHERE fecha = ? AND paciente_id = ?";
        String[] params = new String[]{fecha, DataHolder.getData() };
        Cursor consulta = data_base.rawQuery(c,params);
        while(consulta.moveToNext())
        {
            //Toast.makeText(this, "Comida: "+consulta.getString(0)+", tipo:"+consulta.getString(1)+", valor: "+consulta.getString(2),Toast.LENGTH_SHORT).show();
            if(consulta.getString(0).equals("desayuno")){
                if(consulta.getString(1).equals("antes de")){
                    diary[0]=Integer.parseInt(consulta.getString(2));
                    //Toast.makeText(this,"Desayuno, antes de: "+diary[0],Toast.LENGTH_LONG).show();
                }else{
                    if(consulta.getString(1).equals("despues de")){
                        diary[1]=Integer.parseInt(consulta.getString(2));
                        //Toast.makeText(this,"Desayuno, despues de: "+diary[1],Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(this,"NADA: "+diary[1],Toast.LENGTH_LONG).show();
                    }
                }
            }else{
                if(consulta.getString(0).equals("comida")){
                    if(consulta.getString(1).equals("antes de")){
                        diary[2]=Integer.parseInt(consulta.getString(2));
                        //Toast.makeText(this,"Comida, antes de: "+diary[2],Toast.LENGTH_LONG).show();
                    }else{
                        if(consulta.getString(1).equals("despues de")){
                            diary[3]=Integer.parseInt(consulta.getString(2));
                            //Toast.makeText(this,"Comida, despues de: "+diary[3],Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(this,"Nada 2: "+diary[1],Toast.LENGTH_LONG).show();
                        }
                    }
                }else{
                    if(consulta.getString(0).equals("cena")){
                        if(consulta.getString(1).equals("antes de")){
                            diary[4]=Integer.parseInt(consulta.getString(2));
                            //Toast.makeText(this,"Cena, antes de: "+diary[4],Toast.LENGTH_LONG).show();
                        }else{
                            if(consulta.getString(1).equals("despues de")){
                                diary[5]=Integer.parseInt(consulta.getString(2));
                                //Toast.makeText(this,"Cena, despues de: "+diary[5],Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(this,"Nada 3 "+diary[1],Toast.LENGTH_LONG).show();
                            }

                        }
                    }

                }
            }

        }
        ImageView imageView = new ImageView(this);

        //RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        imageView.setY(30);
        imageView.setX(0);
        //imageView.setLayoutParams(layoutParams);

        layot=(LinearLayout)findViewById(R.id.capa);

        Bitmap imagenBitmap = Bitmap.createBitmap(300,300, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(imagenBitmap);

        Paint pincel = new Paint();
        pincel.setColor(Color.parseColor("#000000"));
        pincel.setTextSize(15);

        /*Dibujando la grafica*/
        canvas.drawLine(20,0,20,260,pincel);
        canvas.drawLine(20,260,300,260,pincel);


        //Dibujando las 6 lineas verticales
        pincel.setColor(Color.parseColor("#B5A9A2"));
        canvas.drawLine(60,0,60,270,pincel);
        canvas.drawLine(100,0,100,270,pincel);
        canvas.drawLine(140,0,140,270,pincel);
        canvas.drawLine(180,0,180,270,pincel);
        canvas.drawLine(220,0,220,270,pincel);
        canvas.drawLine(260,0,260,270,pincel);

        //Dibujando las lineas horizontales
        canvas.drawLine(20,240,300,240,pincel);
        Float f = pincel.getStrokeWidth();
        pincel.setStrokeWidth(3);

        pincel.setColor(Color.parseColor("#FFFF00"));
        canvas.drawLine(20,220,300,220,pincel);
        pincel.setStrokeWidth(f);
        pincel.setColor(Color.parseColor("#B5A9A2"));
        canvas.drawLine(20,200,300,200,pincel);
        canvas.drawLine(20,180,300,180,pincel);
        canvas.drawLine(20,160,300,160,pincel);
        canvas.drawLine(20,140,300,140,pincel);
        canvas.drawLine(20,120,300,120,pincel);
        canvas.drawLine(20,100,300,100,pincel);
        pincel.setColor(Color.parseColor("#FFFF00"));
        pincel.setStrokeWidth(3);
        canvas.drawLine(20,80,300,80,pincel);
        pincel.setStrokeWidth(f);
        pincel.setColor(Color.parseColor("#B5A9A2"));
        canvas.drawLine(20,60,300,60,pincel);
        canvas.drawLine(20,40,300,40,pincel);
        canvas.drawLine(20,20,300,20,pincel);
        pincel.setColor(Color.parseColor("#000000"));


        //canvas.drawColor(Color.parseColor("#E4E4CD"));
        canvas.drawText("Horas", 150,300,pincel);

        //Dibujando las mediciones
        int inicio = 60;
        int alto = 260;
        int xaux1=60,xaux2=0,yaux1=260,yaux2=0;
        for(int i =0; i<6;i++){
            if(diary[i]<=80||diary[i]>=140){
                pincel.setColor(Color.parseColor("#F77123"));
            }else{
                pincel.setColor(Color.parseColor("#000000"));
            }
            yaux2 = alto-((diary[i]-60)*2);
            xaux2 = inicio;
            if(diary[i]>0) {
                canvas.drawCircle(inicio,alto-((diary[i]-60)*2),5, pincel);
                if(yaux2<260){
                    canvas.drawLine(xaux1,yaux1,xaux2,yaux2,pincel);
                    yaux1=yaux2;
                    xaux1 = inicio;
                }
            }
            inicio+=40;
        }
        //canvas.drawCircle(20,100,50, pincel);
        pincel.setTextSize(10);
        canvas.drawText("170mg/dl", 5,40,pincel);
        canvas.drawText("160mg/dl", 5,60,pincel);
        canvas.drawText("150mg/dl", 5,80,pincel);
        canvas.drawText("140mg/dl", 5,100,pincel);
        canvas.drawText("130mg/dl", 5,120,pincel);
        canvas.drawText("120mg/dl", 5,140,pincel);
        canvas.drawText("110mg/dl", 5,160,pincel);
        canvas.drawText("100mg/dl", 5,180,pincel);
        canvas.drawText("90mg/dl", 5,200,pincel);
        canvas.drawText("80mg/dl", 5,220,pincel);
        canvas.drawText("70mg/dl", 5,240,pincel);
        canvas.drawText("60mg/dl", 5,260,pincel);


        canvas.drawText("A. desayuno", 35, 275, pincel);
        canvas.drawText("D. desayuno", 80, 285, pincel);
        canvas.drawText("A. comida", 120, 275, pincel);
        canvas.drawText("D. comida", 160, 285, pincel);
        canvas.drawText("A. cena", 200, 275, pincel);
        canvas.drawText("D. cena", 240, 285, pincel);

        pincel.setColor(Color.parseColor("#D16C31"));
        canvas.drawText("VALOR MAXIMO", 222, 75, pincel);
        canvas.drawText("VALOR MINIMO", 222, 235, pincel);



        imageView.setImageBitmap(imagenBitmap);
        //imageView.setTop(150);
        layot.addView(imageView);
    }




}
package com.example.randallflagg.dmellitus2_app;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.security.MessageDigest;

public class ingresar extends AppCompatActivity {

    EditText userlogin,passlogin;
    String user, pass, hash256pass, consulta;
    Button btn_borrarcampos, btn_ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        userlogin = (EditText)findViewById(R.id.et_userlogin);
        passlogin = (EditText)findViewById(R.id.et_passlogin);
        btn_borrarcampos = (Button)findViewById(R.id.btn_borrarcampos);
        btn_ok = (Button)findViewById(R.id.btn_ok);

    }
    public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public void ingresar_login(View v){


        boolean flag=false;
        String user1= " ", pass1=" ", iduser=" ";
        user = userlogin.getText().toString();
        pass = passlogin.getText().toString();
        hash256pass = sha256(pass);
        String name="";
        base_de_datos db = new base_de_datos(this, "database", null, 1);
        SQLiteDatabase data_base = db.getReadableDatabase();
        String c = "SELECT usuario,contraseña,paciente_id,nombre_p  FROM paciente_db ORDER BY paciente_id";
        Cursor consulta = data_base.rawQuery(c, null);
        while (consulta.moveToNext())
        {
            user1 = consulta.getString(0);
            pass1 = consulta.getString(1);
            iduser = consulta.getString(2);
            name = consulta.getString(3);
            if( user.equals(user1)  && hash256pass.equals(pass1) ){
                data_base.close();
                flag = true;
                //Verificando que exista un doctor asociado

                db = new base_de_datos(this, "database", null, 1);
                data_base = db.getReadableDatabase();
                //final ContentValues registro = new ContentValues();
                c = "SELECT doctor_id  FROM doctorspatient WHERE paciente_id = ? ";
                String[] params = new String[]{iduser};
                consulta = data_base.rawQuery(c, params);
                if (consulta.moveToFirst())
                {
                    int id_doctor = consulta.getInt(0);
                    DataHolder.setData_dr(String.valueOf(id_doctor));
                    DataHolder.setName(name);
                    //data_base = db.getWritableDatabase();
                    //String sql = "INSERT INTO doctorspatient (doctor_id,paciente_id) VALUES ('" + id_doctor + "','" + DataHolder.getData() + "') ";
                    //data_base.execSQL(sql);
                    data_base.close();
                    //Toast.makeText(this, "exito", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this,inicio_u.class);
                    DataHolder.setData(iduser);
                    //Toast.makeText(this, iduser+"entra",Toast.LENGTH_LONG);
                    startActivity(intent);
                }else{
                    DataHolder.setData(iduser);
                    DataHolder.setName(name);
                    data_base.close();
                    Toast.makeText(this, "Antes debes registrar a tu doctor",Toast.LENGTH_LONG).show();
                    Intent sendToRegDr = new Intent(this, registro_doctor.class);
                    startActivity(sendToRegDr);

                }

            }
        }
        consulta.close();
        if(!flag){
            Toast.makeText(this, "Verifica tu usuario o contraseña, o puedes registrarte si eres nuevo!",Toast.LENGTH_LONG).show();
            vaciaCampos(v);
        }

        data_base.close();


    }

    public void vaciaCampos(View v){
        userlogin.setText("");
        passlogin.setText("");
    }
}

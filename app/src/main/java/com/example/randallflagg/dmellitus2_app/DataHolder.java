package com.example.randallflagg.dmellitus2_app;

/**
 * Created by uriel on 29/11/17.
 */

public class DataHolder {
    private static String data="";
    private static String data_dr="";
    private static String namePat="";
    public static String getData() {return data;}
    public static String getData_dr(){return data_dr;}
    public static String getName() {return namePat;}
    public static void setData(String data) {DataHolder.data = data;}
    public static void setData_dr(String data_dr){DataHolder.data_dr = data_dr;}
    public static void setName(String namePat){DataHolder.namePat = namePat;}
}
